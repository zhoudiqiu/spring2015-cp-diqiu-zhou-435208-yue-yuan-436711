Creative.txt and how to use this creative project.

On Windows: Sorry U can’t run our app
On MAC: install python3, pip3 through homebrew, then run pip3 install -r requirement.txt
On Ubuntu: install python3.4dev , python3 and pip, then run python3.4 -m pip install -r requirement.txt

python3 main.py

And your server is on at localhost://8000

Creative description:

All the Handlers in tornado are written in the form of RESTful api.
New user will receive an email containing a code to verify his e-mail before he can use his account.
User can view his account activation status when activating his account
User can request for e-mail resend with a new activation code inside, the old one will be discarded by the database.
User can’t request for more than 3 email resend during the registration process for a single email address
A method to calculate the words typed by the user using regular expression is included in the main page.



Note: I will disable my AWS access key after today’s demo, if you are interested in using this tornado server please replace that with your own AWS access key pairs, thanks.