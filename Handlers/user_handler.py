import tornado
import json
from tornado import gen
from .User import *
from .base_handler import *
import time
import re
from .config import *
import hashlib
from .helper import *
from boto.dynamodb2.table import Table

class UserHandler(BaseHandler):


    @property
    def user_table(self):
        return Table('User_Table',connection=self.dynamo)

    @property
    def user_activate_table(self):
        return Table('User_Activate_Table',connection=self.dynamo)

    @gen.coroutine
    def post(self):

        # Get email from client

        password = self.data['password']
        email = self.data['email'].strip()

        # Hash the username to get an email

        hashed_userid = md5(email)

        # Check if this email has been registered
               
        user_exist = yield gen.maybe_future(self.user_table.has_item(UserID=hashed_userid))

        if user_exist == True:

            # tell client and stop processing this request
            self.write_json({
                'result' : 'fail',
                'reason' : 'email already used'
                })

            return

        hashed_password = hash_password(password)
        
     
        # Create new user item and upload it to database
        yield gen.maybe_future(self.user_table.put_item(data={
                "UserID" : hashed_userid,
                "Email"         : self.data["email"],
                "FirstName"     : self.data['firstname'],
                "LastName"      : self.data['lastname'],
                "AccountActive" : False,
                "Password"      : hashed_password,
            }
        ))
        # Send activate email
        try:
            activate_code = yield gen.maybe_future(
                send_email(
                    self.ses,
                    self.data["email"],
                    self.data["firstname"],
                    self.data["lastname"]
                )
            )
        except:
            self.write_json({
                'result' : 'fail',
                'reason' : 'failed to send email'
            })

        yield gen.maybe_future(self.user_activate_table.put_item(data={
            "UserID" : hashed_userid,
            "Timestamp" : str(time.time()).split(".")[0],
            "Code"      : activate_code,
            "Attempt"   : 1
        }))

        # Only send userid back to the client

        self.write_json({
            'result': 'success',
            'userid': hashed_userid
        })