import tornado
import json
from tornado import gen
from .User import *
from .base_handler import *
import time
import re
from .config import *
from boto.dynamodb2.table import Table


class ArticleHandler(BaseHandler):

	@property
	def article_table(self):
		return Table('User_Article_Table',connection=self.dynamo)

	@gen.coroutine
	def post(self):

		# get the username and text
		text = self.data['text']
		author = self.data['username']
		title = self.data['title']
		textid = md5(str(time.time()).split(".")[0])

		# upload it into the database
		yield gen.maybe_future(self.article_table.put_item(data = {
			"UserID" : textid,
			"author" : author,
			"Title" : title,
			"text" : text
			}
		))
		story_count = yield gen.maybe_future(self.article_table.query_count(index="author-index", author__eq=author))


		#write a json to the frontend
		self.write_json({
			'result' : 'success',
			'count' :story_count
			})
